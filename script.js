// let buttons = ['AC','%','÷','1','2','3','×','4','5','6','-','7','8','9','+','0','='];
//
// let buttonsContainer = document.querySelector('.calculator__buttons');
//
// console.log(555)
// for (let button of buttons) {
//     console.log(333)
//     let btn = "<button class='calculator__btn' value='" + button + " '></button>";
//     buttonsContainer.innerHTML = btn;
// }

let buttonsContainer = document.querySelector(".calculator__buttons");
const btn = document.createElement("div");
btn.classList.add("calculator__btn");
buttonsContainer.appendChild(btn);

"C CE % ÷ 7 8 9 × 4 5 6 - 1 2 3 + 0 ( ) =".split(" ").map((symbol) => {
  btn.insertAdjacentHTML(
    "beforeend",
    `<button value="${symbol}">${symbol}</button>`
  );
});
